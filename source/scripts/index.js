require('./includes/jquery.equal-heights.js');

$(function(){
    if($('#header').length){
        require('./modules/header.js');
    }

    if($('.page-home').length){
        require('./pages/home.js');
    }

    if($('.page-account-dashboard').length){
        require('./pages/account.dashboard.js');
    }

    if($('.page-account-report').length){
        require('./pages/account.report.js');
    }

    if($('.page-account-offers').length){
        require('./pages/account.offers.js');
    }

    if($('.page-account-offer-details').length){
        require('./pages/account.offer-details.js');
    }
});