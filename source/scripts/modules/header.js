initToggle();

function initToggle(){
    $('#header__toggle').on('click', function(){
        $('body').toggleClass('is-menu-expanded');
    });
}