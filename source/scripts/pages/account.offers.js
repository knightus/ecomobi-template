/**
 * @module pageAccountOffers
 */
initFilterForm();

function initFilterForm(){
    $('#page_account_offers__segment_1__form select.chosen').chosen({
        width: '100%',
        disable_search: true
    });
}