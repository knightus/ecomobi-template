/**
 * @module pageHome
 */

initSegment1();

function initSegment1(){
    $('#page_home__segment_1__list').slick({
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        prevArrow: '<a class="slick-prev"></a>',
        nextArrow: '<a class="slick-next"></a>'
    });
}