/**
 * @module: pageOfferDetails
 */

initSegmentsHeights();

$(window).on('resize', function(){
    initSegmentsHeights();
}).resize();

function initSegmentsHeights(){
    $('#page_account_offer_details__segment_1, #page_account_offer_details__segment_2').equalHeights();
}