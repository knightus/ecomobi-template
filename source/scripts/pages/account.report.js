/**
 * @module pageAccountReport
 */
initFilterForm();
initCharts();

function initFilterForm(){
    $('#page_account_report__segment_1__form select.chosen').chosen({
        width: '100%',
        disable_search: true
    });
}

function initCharts(){
    $.each($('.ecomobi.chart:not(".icon")'), function(){
        var $chart = $(this),
            chartType = $chart.data('type'),
            chartSourceVariable = $chart.data('source-variable'),
            $chartCanvas = $chart.find('.canvas'),
            $chartLegend = $chart.find('.legend'),
            chart
        ;

        if(window[chartSourceVariable] && $chartCanvas.length){
            var ctx = $chartCanvas[0].getContext('2d');
            var chartData = window[chartSourceVariable];

            if(chartData.data){
                switch(chartType){
                    case 'line':
                        chart = new Chart(ctx).Line(chartData.data, chartData.settings);
                        break;
                    case 'pie':
                        chart = new Chart(ctx).Pie(chartData.data, chartData.settings);
                        break;
                }
            }
        }

        if($chartLegend.length && chart && chart.generateLegend){
            $chartLegend.html(chart.generateLegend());
        }
    });
}