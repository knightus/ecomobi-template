/*
 * ========================
 * - Plugin: equalHeights -
 * ========================
 */
(function($){
    $.fn.equalHeights = function(){
        var targetHeight = 0;

        $(this).each(function(){
            var currentHeight = $(this).outerHeight();
            if(currentHeight > targetHeight){
                targetHeight = currentHeight;
            }
        });

        $(this).outerHeight(targetHeight);

        return $(this);
    };
}(jQuery));