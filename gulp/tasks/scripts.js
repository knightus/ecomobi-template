var gulp = require('../gulp.js'),
    gutil = require('gulp-util'),
    watch = require('gulp-watch'),
    gulpif = require('gulp-if'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    mainBowerFiles = require('main-bower-files'),
//addsrc = require('gulp-add-src'),
    size = require('gulp-size'),
//filelog = require('gulp-filelog'),
    buffer = require('vinyl-buffer'),
    fs = require('fs'),
    source = require('vinyl-source-stream'),
    browserify = require('browserify'),

    browserSync = require('browser-sync'),
    reload = browserSync.reload,

    glob = require('glob-all'),
    es = require('event-stream'),
    notifier = require('node-notifier'),
    shell = require('gulp-shell'),
    removeComma = require('gulp-trailing-comma')
    ;

var sizeOptions = {
    showFiles: true
};

gulp.task('scripts:vendors:compile', function () {
    return gulp
        .src(mainBowerFiles({
            filter: ["**/*.js"]
        }))
        .pipe(concat('vendors.js'))
        //remove commas
        .pipe(removeComma())
        .pipe(gulp.dest(config.paths.relative.buildScripts))
        .pipe(size(sizeOptions))
        .on('end', reload)
    ;
});

gulp.task('scripts:watch', function scriptsWatch() {
    return gulp
        .watch(config.paths.relative.sourceScripts + '/**/*', ['scripts:compile'])
        ;
});

gulp.task('scripts:jsdoc', shell.task('jsdoc "' + config.paths.absolute.sourceScripts + '" -c jsdoc.configuration.json --verbose'));

gulp.task('scripts:compile', function(done) {
    glob(config.paths.relative.sourceScripts + '/*.js', function (error, files) {
        if (error) done(error);
        var tasks = files.map(function (entry) {
            return browserify({entries: [entry], debug: true})
                .bundle()
                .on('error', function (error) {
                    notifier.notify({message: 'Error: ' + error.message});
                    gutil.log(gutil.colors.red('Error'), error.message);
                    this.emit('end');
                })
                .pipe(source(entry))
                .pipe(buffer())
                .pipe(rename({
                    dirname: '',
                    extname: '.js'
                }))
                .pipe(gulp.dest(config.paths.relative.buildScripts))
                .pipe(size(sizeOptions))
            ;
        });
        es.merge(tasks).on('end', function(){
            reload();
            done();
        });
    });
});

/**
 * - Tasks: Production -
 */

gulp.task('production:scripts:vendors:compile', function () {
    return gulp
        .src(mainBowerFiles({
            filter: ["**/*.js"]
        }))
		.pipe(size(sizeOptions))
        .pipe(concat('vendors.js'))
        //remove commas
        .pipe(removeComma())
        .pipe(uglify())
        .pipe(rename({
            extname: '.min.js'
        }))
        .pipe(gulp.dest(config.paths.relative.buildScripts))
        .pipe(size(sizeOptions))
    ;
});

gulp.task('production:scripts:compile', function(done) {
    glob(config.paths.relative.sourceScripts + '/*.js', function (error, files) {
        if (error) done(error);
        var tasks = files.map(function (entry) {
            return browserify({entries: [entry]})
                .bundle()
                .on('error', function (error) {
                    notifier.notify({message: 'Error: ' + error.message});
                    gutil.log(gutil.colors.red('Error'), error.message);
                    this.emit('end');
                })
                .pipe(source(entry))
                .pipe(buffer())
                .pipe(uglify())
                .pipe(rename({
                    dirname: '',
                    extname: '.min.js'
                }))
                .pipe(gulp.dest(config.paths.relative.buildScripts))
                .pipe(size(sizeOptions))
            ;
        });
        es.merge(tasks).on('end', function(){
            reload();
            done();
        });
    });
});

gulp.task('production:scripts', function(){
    gulp.start('production:scripts:vendors:compile');
    gulp.start('production:scripts:compile');
});