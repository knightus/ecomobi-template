﻿var gulp = require('../gulp.js'),
    gutil = require('gulp-util'),
    gulpif = require('gulp-if'),
    size = require('gulp-size'),
    notifier = require('node-notifier'),
    shell = require('gulp-shell')
    ;

gulp.task('misc:node-version', shell.task('npm -v'));