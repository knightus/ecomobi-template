var gulp = require('../gulp.js'),
	//gutil = require('gulp-util'),
	swig = require('gulp-swig'),
	filter = require('gulp-filter'),
	//filelog = require('gulp-filelog'),
	data = require('gulp-data'),
	path = require('path'),
	changed = require('gulp-changed'),
	//fontgen = require('gulp-fontgen'),
	imagemin = require('gulp-imagemin'),
	pngquant = require('imagemin-pngquant'),
	//spritesmith = require('gulp.spritesmith'),
	merge = require('merge-stream')
	//phantomjssmith = require('phantomjssmith')
;

gulp.task('assets:images:watch',function(){
    return gulp
        .watch(config.paths.relative.sourceImages + '/**/*')
        .on('change',function(file){
            gulp.src(file.path)
                .pipe(imagemin({
                    progressive: true,
                    interlaced: true,
                    svgoPlugins: [{removeViewBox: false}],
                    use: [pngquant()]
                }))
                .pipe(gulp.dest(config.paths.relative.buildImages))
            ;
        })
    ;
});

gulp.task('assets:images:minify-all', function () {
    return gulp
        .src(config.paths.relative.sourceImages + '/**/*')
        //.pipe(changed(config.paths.relative.buildImages))
        //.pipe(imagemin({
        //    progressive: true,
        //    interlaced: true,
        //    svgoPlugins: [{removeViewBox: false}],
        //    use: [pngquant()]
        //}))
        .pipe(gulp.dest(config.paths.relative.buildImages))
    ;
});

/*
gulp.task('assets:spritesheet:generate', function () {
    var spriteData = gulp.src(config.paths.relative.sourceImages + '/icons/*.png').pipe(spritesmith({
        imgName: 'spritesheet.png',
        cssName: 'spritesheet-test.less',
        engine: phantomjssmith
    }));


    // Pipe image stream through image optimizer and onto disk
    var imgStream = spriteData.img
            .pipe(imagemin({
                progressive: true,
                interlaced: true,
                svgoPlugins: [{removeViewBox: false}],
                use: [pngquant()]
            }))
            .pipe(gulp.dest(config.paths.relative.buildImages + '/spritesheets'))
        ;

    // Pipe CSS stream through CSS optimizer and onto disk
    var cssStream = spriteData.css
            .pipe(gulp.dest(config.paths.relative.sourceStyles + '/components'))
        ;


    // Return a merged stream to handle both `end` events
    return merge(imgStream, cssStream)
        ;
});
*/

/*
gulp.task('assets:fonts:generate', function () {
    return gulp
        .src(config.paths.relative.sourceFonts + '/*')
        .pipe(fontgen({
            dest: config.paths.relative.buildFonts
        }))
        ;
});
*/