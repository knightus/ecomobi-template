var gulp = require('../gulp.js'),
//gutil = require('gulp-util'),
    watch = require('gulp-watch'),
    less = require('gulp-less'),
    rename = require('gulp-rename'),
//frep = require('gulp-frep'),
    sakugawa = require('gulp-sakugawa'),
    replace = require('gulp-replace'),
    uncss = require('gulp-uncss')
    ;

gulp.task('styles:watch', function () {
    return gulp
        .watch(config.paths.relative.sourceStyles + '/**/*', ['styles:compile'])
        ;
});

gulp.task('styles:compile', function () {
    return gulp
        .src(config.paths.relative.sourceStyles + '/*.less')
        .pipe(less())
        //.pipe(rename('style_page'))
        .pipe(sakugawa({
            maxSelectors: 4095,
            suffix: '_'
        }))
        .pipe(gulp.dest(config.paths.relative.buildStyles))
        ;
});

/**
 * Convert CSS paths to in-TFS path
 */
gulp.task('styles:convert', function () {
    return gulp
        .src(config.paths.relative.buildStyles + '/*.css')
        .pipe(replace('../img/homepage', '/images'))
        .pipe(replace('../img', '/images'))
        .pipe(replace('../../images', '/images'))
        .pipe(replace('../images', '/images'))
        .pipe(gulp.dest(config.paths.relative.buildStyles + '/output'))
        ;
});

/**
 * Build & remove unused CSS rules
 */
gulp.task('styles:clean-compile',function(){
    return gulp
        .src(config.paths.relative.sourceStyles + '/*.less')
        .pipe(less())
        .pipe(uncss({
            html: [config.paths.relative.build + '/index.html',config.paths.relative.build + '/pages/**.html']
        }))
        .pipe(rename('style'))
        .pipe(sakugawa({
            maxSelectors: 4095,
            suffix: '_'
        }))
        .pipe(gulp.dest(config.paths.relative.buildStyles))
        ;
});