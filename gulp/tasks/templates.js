var gulp = require('../gulp.js'),
    //gutil = require('gulp-util'),
    swig = require('gulp-swig'),
    filter = require('gulp-filter'),
    //filelog = require('gulp-filelog'),
    data = require('gulp-data'),
    path = require('path'),
    fs = require('fs'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload
;

gulp.task('templates:watch', function () {
  return gulp
    .watch(config.paths.relative.sourceTemplates + '/**/*', ['templates:compile'])
    ;
});

gulp.task('templates:compile', function () {
    var getJsonData = function (file) {
        var dataPathObject = path.parse(file.path);
        dataPathObject.dir = dataPathObject.dir.replace(
            path.resolve(config.paths.absolute.sourceTemplates), path.resolve(config.paths.absolute.sourceData)),
            dataPathObject.file = dataPathObject.dir + '/' + path.basename(file.path, path.extname(file.path));
        
        var dataObject = {};
        try{
            var file = fs.statSync(dataPathObject.file + '.json');
            if (file.isFile()) {
                //clear cache if exists
                delete require.cache[require.resolve(dataPathObject.file)];
                dataObject = require(dataPathObject.file + '.json');
            }
        } catch (error) {}
        return dataObject;
    };

    var swigOptions = {
        defaults: {
            cache: false
        }
    };

    return gulp
        .src(config.paths.relative.sourceTemplates + '/**/*.swig')
        .pipe(filter(['**', '!**/layouts/**', '!**/modules/**']))
        .pipe(data(getJsonData))
        .pipe(swig(swigOptions))
        .pipe(gulp.dest(config.paths.absolute.build))
        .on('end', reload)
        ;
});