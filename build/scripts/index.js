(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
/*
 * ========================
 * - Plugin: equalHeights -
 * ========================
 */
(function($){
    $.fn.equalHeights = function(){
        var targetHeight = 0;

        $(this).each(function(){
            var currentHeight = $(this).outerHeight();
            if(currentHeight > targetHeight){
                targetHeight = currentHeight;
            }
        });

        $(this).outerHeight(targetHeight);

        return $(this);
    };
}(jQuery));
},{}],2:[function(require,module,exports){
require('./includes/jquery.equal-heights.js');

$(function(){
    if($('#header').length){
        require('./modules/header.js');
    }

    if($('.page-home').length){
        require('./pages/home.js');
    }

    if($('.page-account-dashboard').length){
        require('./pages/account.dashboard.js');
    }

    if($('.page-account-report').length){
        require('./pages/account.report.js');
    }

    if($('.page-account-offers').length){
        require('./pages/account.offers.js');
    }

    if($('.page-account-offer-details').length){
        require('./pages/account.offer-details.js');
    }
});
},{"./includes/jquery.equal-heights.js":1,"./modules/header.js":3,"./pages/account.dashboard.js":4,"./pages/account.offer-details.js":5,"./pages/account.offers.js":6,"./pages/account.report.js":7,"./pages/home.js":8}],3:[function(require,module,exports){
initToggle();

function initToggle(){
    $('#header__toggle').on('click', function(){
        $('body').toggleClass('is-menu-expanded');
    });
}
},{}],4:[function(require,module,exports){
/**
 * @module pageAccountDashboard
 */
initCharts();

function initCharts(){
    $.each($('.ecomobi.chart:not(".icon")'), function(){
        var $chart = $(this),
            chartType = $chart.data('type'),
            chartSourceVariable = $chart.data('source-variable'),
            $chartCanvas = $chart.find('.canvas'),
            $chartLegend = $chart.find('.legend'),
            chart
        ;

        if(window[chartSourceVariable] && $chartCanvas.length){
            var ctx = $chartCanvas[0].getContext('2d');
            var chartData = window[chartSourceVariable];

            if(chartData.data){
                switch(chartType){
                    case 'line':
                        chart = new Chart(ctx).Line(chartData.data, chartData.settings);
                        break;
                    case 'pie':
                        chart = new Chart(ctx).Pie(chartData.data, chartData.settings);
                        break;
                }
            }
        }

        if($chartLegend.length && chart && chart.generateLegend){
            $chartLegend.html(chart.generateLegend());
        }
    });
}
},{}],5:[function(require,module,exports){
/**
 * @module: pageOfferDetails
 */

initSegmentsHeights();

$(window).on('resize', function(){
    initSegmentsHeights();
}).resize();

function initSegmentsHeights(){
    $('#page_account_offer_details__segment_1, #page_account_offer_details__segment_2').equalHeights();
}
},{}],6:[function(require,module,exports){
/**
 * @module pageAccountOffers
 */
initFilterForm();

function initFilterForm(){
    $('#page_account_offers__segment_1__form select.chosen').chosen({
        width: '100%',
        disable_search: true
    });
}
},{}],7:[function(require,module,exports){
/**
 * @module pageAccountReport
 */
initFilterForm();
initCharts();

function initFilterForm(){
    $('#page_account_report__segment_1__form select.chosen').chosen({
        width: '100%',
        disable_search: true
    });
}

function initCharts(){
    $.each($('.ecomobi.chart:not(".icon")'), function(){
        var $chart = $(this),
            chartType = $chart.data('type'),
            chartSourceVariable = $chart.data('source-variable'),
            $chartCanvas = $chart.find('.canvas'),
            $chartLegend = $chart.find('.legend'),
            chart
        ;

        if(window[chartSourceVariable] && $chartCanvas.length){
            var ctx = $chartCanvas[0].getContext('2d');
            var chartData = window[chartSourceVariable];

            if(chartData.data){
                switch(chartType){
                    case 'line':
                        chart = new Chart(ctx).Line(chartData.data, chartData.settings);
                        break;
                    case 'pie':
                        chart = new Chart(ctx).Pie(chartData.data, chartData.settings);
                        break;
                }
            }
        }

        if($chartLegend.length && chart && chart.generateLegend){
            $chartLegend.html(chart.generateLegend());
        }
    });
}
},{}],8:[function(require,module,exports){
/**
 * @module pageHome
 */

initSegment1();

function initSegment1(){
    $('#page_home__segment_1__list').slick({
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        prevArrow: '<a class="slick-prev"></a>',
        nextArrow: '<a class="slick-next"></a>'
    });
}
},{}]},{},[2])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzb3VyY2Uvc2NyaXB0cy9pbmNsdWRlcy9qcXVlcnkuZXF1YWwtaGVpZ2h0cy5qcyIsInNvdXJjZS9zY3JpcHRzL2luZGV4LmpzIiwic291cmNlL3NjcmlwdHMvbW9kdWxlcy9oZWFkZXIuanMiLCJzb3VyY2Uvc2NyaXB0cy9wYWdlcy9hY2NvdW50LmRhc2hib2FyZC5qcyIsInNvdXJjZS9zY3JpcHRzL3BhZ2VzL2FjY291bnQub2ZmZXItZGV0YWlscy5qcyIsInNvdXJjZS9zY3JpcHRzL3BhZ2VzL2FjY291bnQub2ZmZXJzLmpzIiwic291cmNlL3NjcmlwdHMvcGFnZXMvYWNjb3VudC5yZXBvcnQuanMiLCJzb3VyY2Uvc2NyaXB0cy9wYWdlcy9ob21lLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FDQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3BCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDMUJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ05BO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNuQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDWkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNWQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQzNDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJnZW5lcmF0ZWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIGUodCxuLHIpe2Z1bmN0aW9uIHMobyx1KXtpZighbltvXSl7aWYoIXRbb10pe3ZhciBhPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7aWYoIXUmJmEpcmV0dXJuIGEobywhMCk7aWYoaSlyZXR1cm4gaShvLCEwKTt2YXIgZj1uZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiK28rXCInXCIpO3Rocm93IGYuY29kZT1cIk1PRFVMRV9OT1RfRk9VTkRcIixmfXZhciBsPW5bb109e2V4cG9ydHM6e319O3Rbb11bMF0uY2FsbChsLmV4cG9ydHMsZnVuY3Rpb24oZSl7dmFyIG49dFtvXVsxXVtlXTtyZXR1cm4gcyhuP246ZSl9LGwsbC5leHBvcnRzLGUsdCxuLHIpfXJldHVybiBuW29dLmV4cG9ydHN9dmFyIGk9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtmb3IodmFyIG89MDtvPHIubGVuZ3RoO28rKylzKHJbb10pO3JldHVybiBzfSkiLCIvKlxyXG4gKiA9PT09PT09PT09PT09PT09PT09PT09PT1cclxuICogLSBQbHVnaW46IGVxdWFsSGVpZ2h0cyAtXHJcbiAqID09PT09PT09PT09PT09PT09PT09PT09PVxyXG4gKi9cclxuKGZ1bmN0aW9uKCQpe1xyXG4gICAgJC5mbi5lcXVhbEhlaWdodHMgPSBmdW5jdGlvbigpe1xyXG4gICAgICAgIHZhciB0YXJnZXRIZWlnaHQgPSAwO1xyXG5cclxuICAgICAgICAkKHRoaXMpLmVhY2goZnVuY3Rpb24oKXtcclxuICAgICAgICAgICAgdmFyIGN1cnJlbnRIZWlnaHQgPSAkKHRoaXMpLm91dGVySGVpZ2h0KCk7XHJcbiAgICAgICAgICAgIGlmKGN1cnJlbnRIZWlnaHQgPiB0YXJnZXRIZWlnaHQpe1xyXG4gICAgICAgICAgICAgICAgdGFyZ2V0SGVpZ2h0ID0gY3VycmVudEhlaWdodDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICAkKHRoaXMpLm91dGVySGVpZ2h0KHRhcmdldEhlaWdodCk7XHJcblxyXG4gICAgICAgIHJldHVybiAkKHRoaXMpO1xyXG4gICAgfTtcclxufShqUXVlcnkpKTsiLCJyZXF1aXJlKCcuL2luY2x1ZGVzL2pxdWVyeS5lcXVhbC1oZWlnaHRzLmpzJyk7XHJcblxyXG4kKGZ1bmN0aW9uKCl7XHJcbiAgICBpZigkKCcjaGVhZGVyJykubGVuZ3RoKXtcclxuICAgICAgICByZXF1aXJlKCcuL21vZHVsZXMvaGVhZGVyLmpzJyk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYoJCgnLnBhZ2UtaG9tZScpLmxlbmd0aCl7XHJcbiAgICAgICAgcmVxdWlyZSgnLi9wYWdlcy9ob21lLmpzJyk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYoJCgnLnBhZ2UtYWNjb3VudC1kYXNoYm9hcmQnKS5sZW5ndGgpe1xyXG4gICAgICAgIHJlcXVpcmUoJy4vcGFnZXMvYWNjb3VudC5kYXNoYm9hcmQuanMnKTtcclxuICAgIH1cclxuXHJcbiAgICBpZigkKCcucGFnZS1hY2NvdW50LXJlcG9ydCcpLmxlbmd0aCl7XHJcbiAgICAgICAgcmVxdWlyZSgnLi9wYWdlcy9hY2NvdW50LnJlcG9ydC5qcycpO1xyXG4gICAgfVxyXG5cclxuICAgIGlmKCQoJy5wYWdlLWFjY291bnQtb2ZmZXJzJykubGVuZ3RoKXtcclxuICAgICAgICByZXF1aXJlKCcuL3BhZ2VzL2FjY291bnQub2ZmZXJzLmpzJyk7XHJcbiAgICB9XHJcblxyXG4gICAgaWYoJCgnLnBhZ2UtYWNjb3VudC1vZmZlci1kZXRhaWxzJykubGVuZ3RoKXtcclxuICAgICAgICByZXF1aXJlKCcuL3BhZ2VzL2FjY291bnQub2ZmZXItZGV0YWlscy5qcycpO1xyXG4gICAgfVxyXG59KTsiLCJpbml0VG9nZ2xlKCk7XHJcblxyXG5mdW5jdGlvbiBpbml0VG9nZ2xlKCl7XHJcbiAgICAkKCcjaGVhZGVyX190b2dnbGUnKS5vbignY2xpY2snLCBmdW5jdGlvbigpe1xyXG4gICAgICAgICQoJ2JvZHknKS50b2dnbGVDbGFzcygnaXMtbWVudS1leHBhbmRlZCcpO1xyXG4gICAgfSk7XHJcbn0iLCIvKipcclxuICogQG1vZHVsZSBwYWdlQWNjb3VudERhc2hib2FyZFxyXG4gKi9cclxuaW5pdENoYXJ0cygpO1xyXG5cclxuZnVuY3Rpb24gaW5pdENoYXJ0cygpe1xyXG4gICAgJC5lYWNoKCQoJy5lY29tb2JpLmNoYXJ0Om5vdChcIi5pY29uXCIpJyksIGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgdmFyICRjaGFydCA9ICQodGhpcyksXHJcbiAgICAgICAgICAgIGNoYXJ0VHlwZSA9ICRjaGFydC5kYXRhKCd0eXBlJyksXHJcbiAgICAgICAgICAgIGNoYXJ0U291cmNlVmFyaWFibGUgPSAkY2hhcnQuZGF0YSgnc291cmNlLXZhcmlhYmxlJyksXHJcbiAgICAgICAgICAgICRjaGFydENhbnZhcyA9ICRjaGFydC5maW5kKCcuY2FudmFzJyksXHJcbiAgICAgICAgICAgICRjaGFydExlZ2VuZCA9ICRjaGFydC5maW5kKCcubGVnZW5kJyksXHJcbiAgICAgICAgICAgIGNoYXJ0XHJcbiAgICAgICAgO1xyXG5cclxuICAgICAgICBpZih3aW5kb3dbY2hhcnRTb3VyY2VWYXJpYWJsZV0gJiYgJGNoYXJ0Q2FudmFzLmxlbmd0aCl7XHJcbiAgICAgICAgICAgIHZhciBjdHggPSAkY2hhcnRDYW52YXNbMF0uZ2V0Q29udGV4dCgnMmQnKTtcclxuICAgICAgICAgICAgdmFyIGNoYXJ0RGF0YSA9IHdpbmRvd1tjaGFydFNvdXJjZVZhcmlhYmxlXTtcclxuXHJcbiAgICAgICAgICAgIGlmKGNoYXJ0RGF0YS5kYXRhKXtcclxuICAgICAgICAgICAgICAgIHN3aXRjaChjaGFydFR5cGUpe1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ2xpbmUnOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjaGFydCA9IG5ldyBDaGFydChjdHgpLkxpbmUoY2hhcnREYXRhLmRhdGEsIGNoYXJ0RGF0YS5zZXR0aW5ncyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ3BpZSc6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNoYXJ0ID0gbmV3IENoYXJ0KGN0eCkuUGllKGNoYXJ0RGF0YS5kYXRhLCBjaGFydERhdGEuc2V0dGluZ3MpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYoJGNoYXJ0TGVnZW5kLmxlbmd0aCAmJiBjaGFydCAmJiBjaGFydC5nZW5lcmF0ZUxlZ2VuZCl7XHJcbiAgICAgICAgICAgICRjaGFydExlZ2VuZC5odG1sKGNoYXJ0LmdlbmVyYXRlTGVnZW5kKCkpO1xyXG4gICAgICAgIH1cclxuICAgIH0pO1xyXG59IiwiLyoqXHJcbiAqIEBtb2R1bGU6IHBhZ2VPZmZlckRldGFpbHNcclxuICovXHJcblxyXG5pbml0U2VnbWVudHNIZWlnaHRzKCk7XHJcblxyXG4kKHdpbmRvdykub24oJ3Jlc2l6ZScsIGZ1bmN0aW9uKCl7XHJcbiAgICBpbml0U2VnbWVudHNIZWlnaHRzKCk7XHJcbn0pLnJlc2l6ZSgpO1xyXG5cclxuZnVuY3Rpb24gaW5pdFNlZ21lbnRzSGVpZ2h0cygpe1xyXG4gICAgJCgnI3BhZ2VfYWNjb3VudF9vZmZlcl9kZXRhaWxzX19zZWdtZW50XzEsICNwYWdlX2FjY291bnRfb2ZmZXJfZGV0YWlsc19fc2VnbWVudF8yJykuZXF1YWxIZWlnaHRzKCk7XHJcbn0iLCIvKipcclxuICogQG1vZHVsZSBwYWdlQWNjb3VudE9mZmVyc1xyXG4gKi9cclxuaW5pdEZpbHRlckZvcm0oKTtcclxuXHJcbmZ1bmN0aW9uIGluaXRGaWx0ZXJGb3JtKCl7XHJcbiAgICAkKCcjcGFnZV9hY2NvdW50X29mZmVyc19fc2VnbWVudF8xX19mb3JtIHNlbGVjdC5jaG9zZW4nKS5jaG9zZW4oe1xyXG4gICAgICAgIHdpZHRoOiAnMTAwJScsXHJcbiAgICAgICAgZGlzYWJsZV9zZWFyY2g6IHRydWVcclxuICAgIH0pO1xyXG59IiwiLyoqXHJcbiAqIEBtb2R1bGUgcGFnZUFjY291bnRSZXBvcnRcclxuICovXHJcbmluaXRGaWx0ZXJGb3JtKCk7XHJcbmluaXRDaGFydHMoKTtcclxuXHJcbmZ1bmN0aW9uIGluaXRGaWx0ZXJGb3JtKCl7XHJcbiAgICAkKCcjcGFnZV9hY2NvdW50X3JlcG9ydF9fc2VnbWVudF8xX19mb3JtIHNlbGVjdC5jaG9zZW4nKS5jaG9zZW4oe1xyXG4gICAgICAgIHdpZHRoOiAnMTAwJScsXHJcbiAgICAgICAgZGlzYWJsZV9zZWFyY2g6IHRydWVcclxuICAgIH0pO1xyXG59XHJcblxyXG5mdW5jdGlvbiBpbml0Q2hhcnRzKCl7XHJcbiAgICAkLmVhY2goJCgnLmVjb21vYmkuY2hhcnQ6bm90KFwiLmljb25cIiknKSwgZnVuY3Rpb24oKXtcclxuICAgICAgICB2YXIgJGNoYXJ0ID0gJCh0aGlzKSxcclxuICAgICAgICAgICAgY2hhcnRUeXBlID0gJGNoYXJ0LmRhdGEoJ3R5cGUnKSxcclxuICAgICAgICAgICAgY2hhcnRTb3VyY2VWYXJpYWJsZSA9ICRjaGFydC5kYXRhKCdzb3VyY2UtdmFyaWFibGUnKSxcclxuICAgICAgICAgICAgJGNoYXJ0Q2FudmFzID0gJGNoYXJ0LmZpbmQoJy5jYW52YXMnKSxcclxuICAgICAgICAgICAgJGNoYXJ0TGVnZW5kID0gJGNoYXJ0LmZpbmQoJy5sZWdlbmQnKSxcclxuICAgICAgICAgICAgY2hhcnRcclxuICAgICAgICA7XHJcblxyXG4gICAgICAgIGlmKHdpbmRvd1tjaGFydFNvdXJjZVZhcmlhYmxlXSAmJiAkY2hhcnRDYW52YXMubGVuZ3RoKXtcclxuICAgICAgICAgICAgdmFyIGN0eCA9ICRjaGFydENhbnZhc1swXS5nZXRDb250ZXh0KCcyZCcpO1xyXG4gICAgICAgICAgICB2YXIgY2hhcnREYXRhID0gd2luZG93W2NoYXJ0U291cmNlVmFyaWFibGVdO1xyXG5cclxuICAgICAgICAgICAgaWYoY2hhcnREYXRhLmRhdGEpe1xyXG4gICAgICAgICAgICAgICAgc3dpdGNoKGNoYXJ0VHlwZSl7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAnbGluZSc6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNoYXJ0ID0gbmV3IENoYXJ0KGN0eCkuTGluZShjaGFydERhdGEuZGF0YSwgY2hhcnREYXRhLnNldHRpbmdzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAncGllJzpcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2hhcnQgPSBuZXcgQ2hhcnQoY3R4KS5QaWUoY2hhcnREYXRhLmRhdGEsIGNoYXJ0RGF0YS5zZXR0aW5ncyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZigkY2hhcnRMZWdlbmQubGVuZ3RoICYmIGNoYXJ0ICYmIGNoYXJ0LmdlbmVyYXRlTGVnZW5kKXtcclxuICAgICAgICAgICAgJGNoYXJ0TGVnZW5kLmh0bWwoY2hhcnQuZ2VuZXJhdGVMZWdlbmQoKSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSk7XHJcbn0iLCIvKipcclxuICogQG1vZHVsZSBwYWdlSG9tZVxyXG4gKi9cclxuXHJcbmluaXRTZWdtZW50MSgpO1xyXG5cclxuZnVuY3Rpb24gaW5pdFNlZ21lbnQxKCl7XHJcbiAgICAkKCcjcGFnZV9ob21lX19zZWdtZW50XzFfX2xpc3QnKS5zbGljayh7XHJcbiAgICAgICAgaW5maW5pdGU6IHRydWUsXHJcbiAgICAgICAgc3BlZWQ6IDUwMCxcclxuICAgICAgICBmYWRlOiB0cnVlLFxyXG4gICAgICAgIGNzc0Vhc2U6ICdsaW5lYXInLFxyXG4gICAgICAgIHByZXZBcnJvdzogJzxhIGNsYXNzPVwic2xpY2stcHJldlwiPjwvYT4nLFxyXG4gICAgICAgIG5leHRBcnJvdzogJzxhIGNsYXNzPVwic2xpY2stbmV4dFwiPjwvYT4nXHJcbiAgICB9KTtcclxufSJdfQ==
