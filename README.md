# Ecomobi Template
This is the HTML Template used for building ecomobi.com & its backend site.

# Prerequisites
- `nodejs>=4.4.3`
- `npm>=3.8.7`
- `npm i -g gulp`

# Installation Notes
- `npm i`

# Usage
- Run one of these commands:
    - `gulp`
    - Or any tasks listed in `gulp --tasks` outputs (with `gulp ` prefix)
- Navigate to `localhost:7008`
- Or directly open HTML files that were built in `build/pages`

# Technologies
This application uses a number of open source projects to work properly:

- **[tool]** `Gulp`
- **[tool]** `browserify` (for run nodejs script in browser)
- **[JS]** `jQuery#2.1.4`
- **[JS]** `lodash`
- **[CSS]** `LESS`
- **[CSS]** `bootstrap#3.3.6`
- **[HTML]** `swig`
- **[convention]** `semantic-UI` (for naming convention)
- **[convention]** `BEM` (for naming convention)
- **[convention]** `ITCSS` (for CSS code structure)

# Code Structure

##### Styles
- `overrides`: Include third-party libraries and tweak theirs styles/variables
- `settings`: Include basic styles for fonts, typography, colors...
- `layouts`: Styles for main layouts
- `components`: Styles for small components such as buttons, icons, ...
- `modules`: Styles for modules, which are formed by smaller components
- `pages`: Styles for specific pages

##### Scripts
- `index.js`: Entry point
- `includes`: Self-written plugins/libs
- `pages`: script for specific pages
- `modules`: script for specific modules (`header`)

##### Templates
- `pages`: HTML Template for specific pages, can be extended from `layouts`
- `layouts`: HTML Layouts (temporarily not use)
- `modules`: HTML modules that can be used across pages like `header`, `footer` (temporarily not use)

##### Vendors
Store third-party libraries